<?php

return [

    'users' => [

        'model' => 'Kyaris\Septu\Users\Models\User',
    ],

    'roles' => [

        'model' => 'Kyaris\Septu\Roles\Models\Role'
    ],

    'activations' => [

        'model' => 'Kyaris\Septu\Activations\Models\Activation',
        'expires' => 259200,
        'type' => 'email',
        'uri' => 'auth/activate'
    ],

    'barriers' => [
        'activation'
    ]
];