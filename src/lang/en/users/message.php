<?php

return [

    'error' => [
        'no_login_column' => "No :column was passed.",
        'no_password_column' => 'No password was passed.',
    ]
];