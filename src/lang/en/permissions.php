<?php

return [
    'no_access_to_action' => 'You do not have permission to access [:action]',
    'no_access_to_uri' => 'You do not have permission to access the resource at [:uri]',
];