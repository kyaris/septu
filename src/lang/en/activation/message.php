<?php

return [

    'success' => [
        'activate' => 'Your account has been successfully activated!',
    ],

    'error' => [
        'activate' => 'An error occured while attempting to activate your account.'
    ]
];