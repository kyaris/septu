<?php



Route::get('login', ['as' => 'login', function()
{
    Auth::attempt([
        'email' => 'danielberrytn@gmail.com',
        'password' => 'welcome'
    ]);

    return redirect()->intended();
}]);

Route::group(['prefix' => 'septu'], function()
{
    Route::group(['prefix' => 'users', 'middleware' => 'septu.permissions'], function()
    {
        Route::get('create', function()
        {
            $user = Auth::getProvider()->create([
                'email'      => 'danielberrytn@gmail.com',
                'password'   => 'welcome',
            ]);

        });

        Route::get('show', function()
        {
            $user = Auth::getUserProvider()->retrieve(1);

            dd($user->hasRole('admin'));
        });

        Route::get('permissions', function()
        {
            $user = Auth::getUserProvider()->retrieve(1);

            dd($user->hasAccess('septu/resources'));
        });

        Route::get('roles', function()
        {
            $user = Auth::getUserProvider()->retrieve(1);

            dd($user->hasAnyRole(['grafter', 'careless', 'admin']));
        });
    });

    Route::group(['prefix' => 'roles'], function()
    {
        Route::get('/', function()
        {
            dd(Auth::getRoleProvider());
        });

        Route::get('create', function()
        {
            $role = Auth::getRoleProvider()->create([
                'slug' => 'user',
                'name' => 'Users',
            ]);

            dd($role);

        });
    });

    Route::group(['prefix' => 'resources'], function()
    {
        Route::get('/', function()
        {
            $resources = [
                'kyaris/users' => [
                    'septu/resources' => 'Create new users',
                    'users.delete' => 'Delete Users',
                    'Kyaris\Users\Http\Controllers\Frontend\UsersController@index' => 'View Users'
                ]
            ];

            $resourceProvider = app()['septu.resources'];

            $resourceProvider->addResources($resources);

            dd($resourceProvider->getResourceBag()->all());
        });
    });
});