<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

    <h2>Your <strong>{{ config('site.title') }}</strong> Account</h2>

    <p>Hello {{ $user->first_name }},</p>

    <p>
        Your account has been created, but before you can login, you will need to visit the following
        link to activate your account:
    </p>

    <p>
        <a href="{{ $activationLink }}">{{ $activationLink }}</a>
    </p>

    <p>Please keep a copy of this email for your records.</p>

    <p>-----------------------------------------------------</p>

    <p><strong>Email:</strong> {{ $user->email }}</p>
    <p><strong>Website Url:</strong> {{ url('/') }}</p>
    <p><strong>Login Page:</strong> {{ route('auth.login') }}</p>

    <p>-----------------------------------------------------</p>

    <p>
        For your safety and the overall security of the website, your password will not be displayed here.
        If at anytime you forget your password, you can reset it by visiting:
    </p>

    <p>
        <a href="{{ route('password.email') }}">{{ route('password.email') }}</a>
    </p>

    <p>
        Thank you for becoming a valued member of {{ config('site.brand_name') }}.
    </p>

    <p>
        Sincerely, <br/>
        The {{ config('site.title') }} Team
    </p>
</body>
</html>
