<?php namespace Kyaris\Septu\Roles\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Kyaris\Septu\Acl\Permissions\Traits\PermissionsTrait;

class Role extends Model implements RoleInterface {

    /**
     * Name of our table
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * Name of our user's model
     *
     * @var string
     */
    protected $usersModel = 'Kyaris\Septu\Users\Models\User';

    /**
     * @{inheritDoc}
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * Relationship with the user's table.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany($this->usersModel, 'roles_users', 'role_id', 'user_id');
    }

    /**
     * Returns the unique key of the role
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->getKey();
    }

    /**
     * Returns the role's unique slug
     *
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Returns the name of the role
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the name of our user's model
     *
     * @return string
     */
    public function getUsersModel()
    {
        return $this->usersModel;
    }

    /**
     * Sets the name of our user's model
     *
     * @param string $usersModel
     */
    public function setUsersModel($usersModel)
    {
        $this->usersModel = $usersModel;
    }

    /**
     * Mutator for our slug attribute
     *
     * Makes sure that our slug is an actual slug.
     *
     * @param $slug
     */
    public function setSlugAttribute($slug)
    {
        $this->attributes['slug'] = Str::slug($slug);
    }


    /**
     * Get mutator for the "permissions" attribute.
     *
     * @param  mixed  $permissions
     * @return array
     */
    public function getPermissionsAttribute($permissions)
    {
        return $permissions ? json_decode($permissions, true) : [];
    }

    /**
     * Set mutator for the "permissions" attribute.
     *
     * @param  mixed  $permissions
     * @return void
     */
    public function setPermissionsAttribute(array $permissions)
    {
        $this->attributes['permissions'] = $permissions ? json_encode($permissions) : '';
    }
}