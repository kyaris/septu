<?php namespace Kyaris\Septu\Roles\Repositories;

use Illuminate\Contracts\Events\Dispatcher;
use Kyaris\Support\Traits\Eventable;
use Kyaris\Support\Traits\RepositoryTrait;

class EloquentRoleRepository implements RoleRepositoryInterface {

    use RepositoryTrait, Eventable;

    /**
     * Name of our role model
     *
     * @var string
     */
    protected $model = 'Kyaris\Septu\Roles\Models\Role';

    /**
     * Creates a new instance of our Eloquent Role Repository
     *
     * @param null $model
     */
    public function __construct(Dispatcher $dispatcher, $model = null)
    {
        $this->dispatcher = $dispatcher;

        if ( ! is_null($model))
        {
            $this->model = $model;
        }
    }

    /**
     * Retrieves a role by it's id or returns the given
     * instance
     *
     * @param $id
     * @return mixed
     */
    public function retrieve($id)
    {
        if ( $id instanceof RoleModel)
        {
            return $id;
        }

        return $this->retrieveById($id);
    }

    /**
     * Retrieves a role by it's primary key (id)
     *
     * @param $id
     * @return mixed
     */
    public function retrieveById($id)
    {
        return $this->createModel()
            ->newQuery()
            ->retrieve($id);
    }

    /**
     * Retrieves a role by it's unique slug
     *
     * @param $slug
     * @return mixed
     */
    public function retrieveBySlug($slug)
    {
        return $this
            ->createModel()
            ->newQuery()
            ->where('slug', '=', $slug);
    }

    /**
     * Creates a new role
     *
     * @param $attributes
     * @param bool $activate
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($attributes, $activate = false)
    {
        $role = $this->createModel();

        $this->fireEvent('SeptuCreatingNewRole', $role);

        $this->ensureRequiredAttributesArePresent($attributes);

        $role->fill($attributes)->save();

        $this->fireEvent('SeptuFinishedCreatingRole', compact('role', 'attributes'));

        return $role;
    }

    /**
     * Updates an existing role
     *
     * @param RoleModelInterface|int $id
     * @param array $attributes
     * @param bool $activate
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, $attributes, $activate = false)
    {
        $role = $this->retrieve($id);

        $this->fireEvent('SeptuUpdatingRole', compact('role', 'attributes'));

        $this->ensureRequiredAttributesArePresent($attributes);

        $role->fill($attributes)->save();

        $this->fireEvent('SeptuFinishedUpdatingRole', compact('role', 'attributes'));

        return $role;
    }

    /**
     * Ensures that we have passed the required login and password fields.
     *
     * Any user that is created or updated must have at a minimum the login
     * column and password fields specified. If either is missing, and exception
     * will be thrown.
     *
     * An event is fired off here that will allow you to specify your own
     * set of required attributed if needed.
     *
     * Here is an example of using an event to add your own required fields.
     *
     * <code>
     * Event::listen('SeptuEnsuringRequiredRoleFieldsArePresent', function($attributes)
     * {
     *     if (empty($attributes['description']))
     *     {
     *         $exception = "You must specify a description for this role.";
     *         throw new InvalidArgumentException($exception);
     *     }
     * });
     * </code>
     *
     * @param $attributes
     * @throws InvalidArgumentException
     */
    protected function ensureRequiredAttributesArePresent($attributes)
    {
        if (empty($attributes['slug']))
        {
            $exception = trans('kyaris/septu::roles/message.error.no_slug_provided');
            throw new InvalidArgumentException($exception);
        }

        if (empty($attributes['name']))
        {
            $exception = trans('kyaris/septu::roles/message.error.no_name_provided');
            throw new InvalidArgumentException($exception);
        }

        $this->fireEvent('SeptuEnsuringRequiredRoleFieldsArePresent', $attributes);
    }
}