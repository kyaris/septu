<?php namespace Kyaris\Septu\Roles\Traits;

trait RoleableTrait {

    /**
     * The name of our group model
     *
     * @var string
     */
    protected $rolesModel = 'Kyaris\Septu\Roles\Models\Role';

    /**
     * Relationship with the roles's table.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany($this->rolesModel, 'roles_users', 'user_id', 'role_id');
    }

    /**
     * Returns our collection of roles
     *
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Checks to see if a user has the given role
     *
     * You can pass either an
     * @param $role
     * @return bool
     */
    public function hasRole($role)
    {
        $role = array_first($this->roles, function($index, $instance) use ($role)
        {
            if ($role instanceof RoleModelInterface)
            {
                return ($instance->getId() === $role->getId());
            }

            if ($instance->getId() == $role || $instance->getSlug() == $role)
            {
                return true;
            }

            return false;
        });

        return $role !== null;
    }

    /**
     * Checks to see if the user has any of the given roles
     *
     * @param array $roles
     * @return bool
     */
    public function hasAnyRole(array $roles)
    {
        foreach ($roles as $role)
        {
            if ($this->hasRole($role))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the name of the role model
     *
     * @return string
     */
    public function getRolesModel()
    {
        return $this->rolesModel;
    }

    /**
     * Sets the name of the role model
     *
     * @param string $rolesModel
     */
    public function setRolesModel($rolesModel)
    {
        $this->rolesModel = $rolesModel;
    }
}