<?php namespace Kyaris\Septu\Acl\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Contracts\Routing\ResponseFactory;

class PermissionsMiddleware implements Middleware {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * The response factory implementation.
     *
     * @var ResponseFactory
     */
    protected $response;

    /**
     * Create a new filter instance
     * @param Guard $auth
     * @param ResponseFactory $response
     */
    public function __construct(Guard $auth, ResponseFactory $response)
    {
        $this->auth = $auth;
        $this->response = $response;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth = $this->auth;

        if ($auth->guest())
        {
            return $this->response->redirectToRoute('login');
        }

        $user = $this->auth->user();

        $route = $request->route();

        if ($route->getActionName() === 'Closure')
        {
            $uri = $request->route()->getUri();

            if ($user->hasAccess($uri))
            {
                return $next($request);
            }

            $error = trans("kyaris/septu::permissions.no_access_to_uri", compact('uri'));
        }
        else
        {
            $action = $route->getActionName();

            if ($user->hasAccess($action))
            {
                return $next($request);
            }

            $error = trans("kyaris/septu::permissions.no_access_to_action", compact('action'));
        }

        return $this->response->make($error, 401);
    }
}
