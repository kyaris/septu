<?php namespace Kyaris\Septu\Acl\Permissions;

class Permissions {

    /**
     * The permissions.
     *
     * @var array
     */
    protected $permissions = [ ];

    /**
     * The secondary permissions.
     *
     * @var array
     */
    protected $secondaryPermissions = [ ];

    /**
     * An array of cached, prepared permissions.
     *
     * @var array
     */
    protected $preparedPermissions;

    /**
     * Create a new permissions instance.
     *
     * @param  array $permissions
     * @param  array $secondaryPermissions
     * @return \Kyaris\Septu\Acl\Permissions\Permissions
     */
    public function __construct(array $permissions = null, array $secondaryPermissions = null)
    {
        if ( isset($permissions) )
        {
            $this->permissions = $permissions;
        }

        if ( isset($secondaryPermissions) )
        {
            $this->secondaryPermissions = $secondaryPermissions;
        }
    }

    /**
     * Checks to see if the entity has the given permission
     *
     * @param $permissions
     * @return bool
     */
    public function hasAccess($permissions)
    {
        if ( is_string($permissions) )
        {
            $permissions = func_get_args();
        }

        $prepared = $this->getPreparedPermissions();

        foreach ( $permissions as $permission )
        {
            if ( ! $this->checkPermission($prepared, $permission) )
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks to see if the entity has any of the given permissions
     *
     * @param $permissions
     * @return bool
     */
    public function hasAnyAccess($permissions)
    {
        if ( is_string($permissions) )
        {
            $permissions = func_get_args();
        }

        $prepared = $this->getPreparedPermissions();

        foreach ( $permissions as $permission )
        {
            if ( $this->checkPermission($prepared, $permission) )
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Grabs the prepared permissions
     *
     * @return array
     */
    protected function getPreparedPermissions()
    {
        if ( $this->preparedPermissions === null )
        {
            $this->preparedPermissions = $this->createPreparedPermissions();
        }

        return $this->preparedPermissions;
    }

    /**
     * Returns the secondary permissions.
     *
     * @return array
     */
    public function getSecondaryPermissions()
    {
        return $this->secondaryPermissions;
    }

    /**
     * Sets secondary permissions.
     *
     * @param  array $secondaryPermissions
     * @return void
     */
    public function setSecondaryPermissions(array $secondaryPermissions)
    {
        $this->secondaryPermissions = $secondaryPermissions;

        $this->preparedPermissions = null;
    }

    /**
     * {@inheritDoc}
     */
    protected function createPreparedPermissions()
    {
        $prepared = [ ];

        if ( ! empty($this->secondaryPermissions) )
        {
            foreach ( $this->secondaryPermissions as $permissions )
            {
                $this->preparePermissions($prepared, $permissions);
            }
        }

        if ( ! empty($this->permissions) )
        {
            $permissions = [ ];

            $this->preparePermissions($permissions, $this->permissions);

            $prepared = array_merge($prepared, $permissions);
        }

        return $prepared;
    }

    /**
     * Does the heavy lifting of preparing permissions.
     *
     * @param  array $prepared
     * @param  array $permissions
     * @return void
     */
    protected function preparePermissions(array &$prepared, array $permissions)
    {
        foreach ( $permissions as $keys => $value )
        {
            foreach ( $this->extractClassPermissions($keys) as $key )
            {
                // If the value is not in the array, we're opting in
                if ( ! array_key_exists($key, $prepared) )
                {
                    $prepared[ $key ] = $value;

                    continue;
                }

                // If our value is in the array and equals false, it will override
                if ( $value === false )
                {
                    $prepared[ $key ] = $value;
                }
            }
        }
    }

    /**
     * Takes the given permission key and inspects it for a class & method. If
     * it exists, methods may be comma-separated, e.g. Class@method1,method2.
     *
     * @param  string $key
     * @return array
     */
    protected function extractClassPermissions($key)
    {
        if ( ! str_contains($key, '@') )
        {
            return (array) $key;
        }

        $keys = [ ];

        list($class, $methods) = explode('@', $key);

        foreach ( explode(',', $methods) as $method )
        {
            $keys[] = "{$class}@{$method}";
        }

        return $keys;
    }

    /**
     * Checks a permission in the prepared array, including wildcard checks and permissions.
     *
     * @param  array $prepared
     * @param  string $permission
     * @return bool
     */
    protected function checkPermission(array $prepared, $permission)
    {
        if ( array_key_exists($permission, $prepared) && $prepared[ $permission ] === true )
        {
            return true;
        }

        foreach ( $prepared as $key => $value )
        {
            if ( (str_is($permission, $key) || str_is($key, $permission)) && $value === true )
            {
                return true;
            }
        }

        return false;
    }

    public function getPermissions()
    {
        return $this->getPreparedPermissions();
    }
}