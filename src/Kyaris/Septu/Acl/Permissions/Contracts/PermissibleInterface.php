<?php namespace Kyaris\Septu\Acl\Permissions\Contracts;

interface PermissibleInterface {

    /**
     * Creates a new permissions object
     *
     * @return mixed
     */
    public function createPermissions();
}