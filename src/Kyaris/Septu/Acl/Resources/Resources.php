<?php namespace Kyaris\Septu\Resources;

class Resources {

    /**
     * Resource bag instance
     *
     * @var ResourceBag
     */
    protected $resourceBag;

    /**
     * Creates a new instance of our resource repository
     *
     * @param ResourceBag $resourceBag
     */
    public function __construct(ResourceBag $resourceBag)
    {
        $this->resourceBag = $resourceBag;
    }
    /**
     * Adds an array of resources to the resource bag
     *
     * @param array $resources
     */
    public function addResources( array $resources = [])
    {
        $this->resourceBag->add($resources);
    }

    public function getResourceBag()
    {
        return $this->resourceBag;
    }
}