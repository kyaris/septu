<?php namespace Kyaris\Septu\Resources;

use Illuminate\Support\Collection;

class ResourceBag extends Collection {

    /**
     * Adds an array of resources to the resource bag
     *
     * @param array $resources
     */
    public function add( array $resources)
    {
        foreach($resources as $package)
        {
            foreach($package as $slug=>$description)
            {
                $this->items[$slug] = $description;
            }
        }
    }
}