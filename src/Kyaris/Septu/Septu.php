<?php namespace Kyaris\Septu;

use Illuminate\Auth\Guard;
use Illuminate\Auth\UserProviderInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard as GuardContract;
use Illuminate\Contracts\Auth\User;
use Illuminate\Session\Store as SessionStore;
use Kyaris\Septu\Activations\Repositories\ActivationRepositoryInterface;
use Kyaris\Septu\Barriers\BarrierInterface;
use Kyaris\Septu\Roles\Repositories\RoleRepositoryInterface;
use Kyaris\Septu\Users\Repositories\UserRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;

class Septu extends Guard implements GuardContract
{

    /**
     * Instance of our role provider
     *
     * @var RoleRepositoryInterface
     */
    protected $roleProvider;

    /**
     * Instance of our activation provider
     *
     * @var ActivationRepositoryInterface
     */
    protected $activationProvider;

    /**
     * Array containing our enabled authentication barriers
     *
     * @var array
     */
    protected $barriers = [];

    /**
     *
     * @var bool
     */
    protected $barriersEnabled = true;

    /**
     * Creates a new instance of Septu
     *
     * @param UserRepositoryInterface $provider
     * @param RoleRepositoryInterface $roleProvider
     * @param ActivationRepositoryInterface $activationProvider
     * @param SessionStore $session
     * @param Request $request
     */
    public function __construct(
        UserRepositoryInterface $provider,
        RoleRepositoryInterface $roleProvider,
        ActivationRepositoryInterface $activationProvider,
        SessionStore $session,
        Request $request = null
    )
    {
        parent::__construct($provider, $session, $request);

        $this->roleProvider = $roleProvider;

        $this->activationProvider = $activationProvider;
    }

    /**
     * Log a user into the application.
     *
     * @param Authenticatable|User $user
     * @param  bool $remember
     * @return void
     */
    public function login(Authenticatable $user, $remember = false)
    {

        if (!$this->checkBarriers('login', $user)) {
            return false;
        }

        $this->updateSession($user->getAuthIdentifier());

        // If the user should be permanently "remembered" by the application we will
        // queue a permanent cookie that contains the encrypted copy of the user
        // identifier. We will then decrypt this later to retrieve the users.
        if ($remember) {
            $this->createRememberTokenIfDoesntExist($user);

            $this->queueRecallerCookie($user);
        }

        // If we have an event dispatcher instance set we will fire an event so that
        // any listeners will hook into the authentication events and run actions
        // based on the login and logout events fired from the guard instances.
        if (isset($this->events)) {
            $this->events->fire('auth.login', array($user, $remember));
        }

        $this->setUser($user);
    }

    /**
     * Registers a new user
     *
     * @param array $attributes
     * @param bool $activate
     * @return mixed
     */
    public function register(array $attributes, $activate = false)
    {
        $user = $this->getUserProvider()->create($attributes, $activate);

        $this->events->fire('SeptuNewUserRegistered', $user);

        return $user;
    }

    /**
     * Registers and activates a new user account.
     *
     * @param array $attributes
     * @return mixed
     */
    public function registerAndActivateUser(array $attributes)
    {
        return $this->register($attributes, true);
    }

    /**
     * Alias for getProvider()
     *
     * This is only here so we have some common naming convention
     * between our provider methods (e.g. getRoleProvider, getUserProvider, etc...)
     *
     * @return UserProviderInterface
     */
    public function getUserProvider()
    {
        return $this->getProvider();
    }

    /**
     * Alias for setProvider()
     *
     * This is only here so we have some common naming convention
     * between our provider methods (e.g. setRoleProvider, setUserProvider, etc...)
     *
     * @param UserProviderInterface $provider
     * @return UserProviderInterface
     */
    public function setUserProvider(UserProviderInterface $provider)
    {
        $this->setProvider($provider);
    }

    /**
     * Returns our instance of the role provider
     *
     * @return RoleRepositoryInterface
     */
    public function getRoleProvider()
    {
        return $this->roleProvider;
    }

    /**
     * Sets our instance of the role provider.
     *
     * @param RoleRepositoryInterface $roleProvider
     */
    public function setRoleProvider($roleProvider)
    {
        $this->roleProvider = $roleProvider;
    }

    /**
     * Returns our activation provider
     *
     * @return ActivationRepositoryInterface
     */
    public function getActivationProvider()
    {
        return $this->activationProvider;
    }

    /**
     * Sets our activation provider instance
     *
     * @param ActivationRepositoryInterface $activationProvider
     */
    public function setActivationProvider($activationProvider)
    {
        $this->activationProvider = $activationProvider;
    }

    /**
     * Checks to see if our authentication barriers are enabled
     *
     * @return bool
     */
    public function areBarriersEnabled()
    {
        return $this->barriersEnabled;
    }

    /**
     * Enables our authentication barriers
     */
    public function enableBarriers()
    {
        $this->barriersEnabled = true;
    }

    /**
     * Disables our authentication barriers
     */
    public function disableBarriers()
    {
        $this->barriersEnabled = false;
    }

    /**
     * Enables a new authentication barrier
     *
     * @param $key
     * @param BarrierInterface $barrier
     */
    public function enableBarrier($key, BarrierInterface $barrier)
    {
        $this->barriers[$key] = $barrier;
    }

    /**
     * Disables an authentication barrier
     * @param $key
     */
    public function disableBarrier($key)
    {
        if ( isset($this->barriers[$key]))
        {
            unset($this->barriers[$key]);
        }
    }

    /**
     * Checks all of our enabled barriers for a given method.
     *
     * @param $method
     * @param Authenticatable $user
     * @param bool $halt
     * @return bool
     */
    public function checkBarriers($method, Authenticatable $user, $halt = true)
    {
        if ( ! $this->areBarriersEnabled())
        {
            return true;
        }

        foreach ($this->barriers as $barrier)
        {
            $response = $barrier->{$method}($user);

            if ( $response === false && $halt === true)
            {
                return false;
            }
        }

        return true;
    }
}