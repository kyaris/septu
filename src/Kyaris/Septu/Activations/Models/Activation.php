<?php namespace Kyaris\Septu\Activations\Models;

use Illuminate\Database\Eloquent\Model;

class Activation extends Model {

    /**
     * @{inheritDoc}
     */
    protected $table = 'activations';

    /**
     * @{inheritDoc}
     */
    protected $guarded = ['id', 'created_at', 'updated_at', 'user_id'];

    /**
     * Accessor for the completed attribute.
     *
     * @param  mixed  $completed
     * @return bool
     */
    public function getCompletedAttribute($completed)
    {
        return (bool) $completed;
    }

    /**
     * Mutator for the completed attribute.
     *
     * @param  mixed  $completed
     * @return void
     */
    public function setCompletedAttribute($completed)
    {
        $this->attributes['completed'] = (bool) $completed;
    }
}