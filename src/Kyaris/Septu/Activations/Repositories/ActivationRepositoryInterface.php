<?php namespace Kyaris\Septu\Activations\Repositories;

use Illuminate\Contracts\Auth\Authenticatable;

interface ActivationRepositoryInterface {

    /**
     * Create a new activation record and code.
     *
     * @param  Authenticatable $user
     * @return string
     */
    public function create(Authenticatable $user);

    /**
     * Checks if a valid activation exists for the given user
     *
     * @param Authenticatable $user
     * @param null $code
     * @return mixed
     */
    public function exists(Authenticatable $user, $code = null);

    /**
     * Completes the activation for the given user.
     *
     * @param  Authenticatable $user
     * @param  string  $code
     * @return bool
     */
    public function complete(Authenticatable $user, $code);

    /**
     * Checks if a valid activation has been completed.
     *
     * @param  Authenticatable $user
     * @return bool
     */
    public function completed(Authenticatable $user);

    /**
     * Remove an existing activation (deactivate).
     *
     * @param  Authenticatable $user
     * @return bool|null
     */
    public function remove(Authenticatable $user);

    /**
     * Remove expired activation codes.
     *
     * @return int
     */
    public function removeExpired();
}