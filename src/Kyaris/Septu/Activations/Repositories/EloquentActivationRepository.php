<?php namespace Kyaris\Septu\Activations\Repositories;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Kyaris\Support\Traits\RepositoryTrait;

class EloquentActivationRepository implements ActivationRepositoryInterface {

    use RepositoryTrait;

    /**
     * The Eloquent activation model name.
     *
     * @var string
     */
    protected $model = 'Kyaris\Septu\Activations\Models\Activation';

    /**
     * The activation expiration time, in seconds.
     *
     * @var int
     */
    protected $expires = 259200;

    /**
     * Creates a new instance of our activation repository
     *
     * @param null $model
     * @param null $expires
     */
    public function __construct($model = null, $expires = null)
    {
        if (isset($model))
        {
            $this->model = $model;
        }

        if (isset($expires))
        {
            $this->expires = $expires;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function create(Authenticatable $user)
    {
        $activation = $this->createModel();

        $code = $this->generateActivationCode();

        $activation->fill(compact('code'));

        $activation->user_id = $user->getAuthIdentifier();

        $activation->save();

        return $activation;
    }

    /**
     * {@inheritDoc}
     */
    public function exists(Authenticatable $user, $code = null)
    {
        $expires = $this->expires();

        $activation = $this
            ->createModel()
            ->newQuery()
            ->where('user_id', $user->getAuthIdentifier())
            ->where('completed', false)
            ->where('created_at', '>', $expires);

        if ($code)
        {
            $activation->where('code', $code);
        }

        return $activation->first() ?: false;
    }

    /**
     * {@inheritDoc}
     */
    public function complete(Authenticatable $user, $code)
    {
        $expires = $this->expires();

        $activation = $this
            ->createModel()
            ->newQuery()
            ->where('user_id', $user->getAuthIdentifier())
            ->where('code', $code)
            ->where('completed', false)
            ->where('created_at', '>', $expires)
            ->first();

        if ($activation === null)
        {
            return false;
        }

        $activation->fill([
            'completed'    => true,
            'completed_at' => Carbon::now(),
        ]);

        $activation->save();

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function completed(Authenticatable $user)
    {
        $activation = $this
            ->createModel()
            ->newQuery()
            ->where('user_id', $user->getAuthIdentifier())
            ->where('completed', true)
            ->first();

        return $activation ?: false;
    }

    /**
     * {@inheritDoc}
     */
    public function remove(Authenticatable $user)
    {
        $activation = $this->completed($user);

        if ($activation === false)
        {
            return false;
        }

        return $activation->delete();
    }

    /**
     * {@inheritDoc}
     */
    public function removeExpired()
    {
        $expires = $this->expires();

        return $this
            ->createModel()
            ->newQuery()
            ->where('completed', false)
            ->where('created_at', '<', $expires)
            ->delete();
    }

    /**
     * Returns the expiration date.
     *
     * @return \Carbon\Carbon
     */
    protected function expires()
    {
        return Carbon::now()->subSeconds($this->expires);
    }

    /**
     * Return a random string for an activation code.
     *
     * @return string
     */
    protected function generateActivationCode()
    {
        return str_random(32);
    }
}