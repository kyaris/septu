<?php namespace Kyaris\Septu\Barriers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\User;
use Kyaris\Septu\Barriers\Exceptions\NotActivatedException;
use Kyaris\Septu\Activations\Repositories\ActivationRepositoryInterface;

class ActivationBarrier implements BarrierInterface {

    /**
     * The activation repository.
     *
     * @var \Kyaris\Septu\Activations\Repositories\ActivationRepositoryInterface
     */
    protected $activations;

    /**
     * Create a new activation checkpoint.
     *
     * @param  ActivationRepositoryInterface $activations
     */
    public function __construct(ActivationRepositoryInterface $activations)
    {
        $this->activations = $activations;
    }

    /**
     * {@inheritDoc}
     */
    public function login(Authenticatable $user)
    {
        return $this->checkActivation($user);
    }

    /**
     * {@inheritDoc}
     */
    public function check(Authenticatable $user)
    {
        return $this->checkActivation($user);
    }

    /**
     * Checks the activation status of the given user.
     *
     * @param  User $user
     * @return bool
     * @throws \Kyaris\Septu\Activations\Exceptions\NotActivatedException
     */
    protected function checkActivation(Authenticatable $user)
    {
        $completed = $this->activations->completed($user);

        if ( ! $completed)
        {
            $exception = new NotActivatedException('Your account has not been activated yet.');

            $exception->setUser($user);

            throw $exception;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function fail(Authenticatable $user = null) {}
}