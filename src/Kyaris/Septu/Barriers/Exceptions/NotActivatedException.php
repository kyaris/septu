<?php namespace Kyaris\Septu\Barriers\Exceptions;

use Illuminate\Contracts\Auth\Authenticatable;
use RuntimeException;

class NotActivatedException extends RuntimeException {

    /**
     * The user which caused the exception.
     *
     * @var \Illuminate\Contracts\Auth\Authenticatable
     */
    protected $user;

    /**
     * Returns the user.
     *
     * @return Authenticatable
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets the user associated with Sentinel (does not log in).
     *
     * @param Authenticatable $user
     * @return void
     */
    public function setUser(Authenticatable $user)
    {
        $this->user = $user;
    }
}