<?php namespace Kyaris\Septu\Barriers;

use Illuminate\Contracts\Auth\Authenticatable;

interface BarrierInterface {

    /**
     * Checkpoint after a user is logged in. Return false to deny persistence.
     *
     * @param  Authenticatable $user
     * @return bool
     */
    public function login(Authenticatable $user);

    /**
     * Checkpoint for when a user is currently stored in the session.
     *
     * @param  Authenticatable $user
     * @return bool
     */
    public function check(Authenticatable $user);

    /**
     * Checkpoint for when a failed login attempt is logged. User is not always
     * passed and the result of the method will not affect anything, as the
     * login failed.
     *
     * @param  Authenticatable $user
     * @return void
     */
    public function fail(Authenticatable $user = null);
}