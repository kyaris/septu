<?php namespace Kyaris\Septu\Users\Repositories;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Auth\UserProviderInterface;
use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\User as UserContract;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Hashing\Hasher;
use InvalidArgumentException;
use Kyaris\Support\Traits\Eventable;
use Kyaris\Support\Traits\RepositoryTrait;

class EloquentUserRepository extends EloquentUserProvider implements UserProviderInterface, UserRepositoryInterface {

    use RepositoryTrait, Eventable;

    /**
     * Name of the user's model
     * @var string
     */
    protected $model = 'Kyaris\Septu\Users\Models\User';

    /**
     * Creates a new instance of our eloquent user provider
     *
     * @param Container $container
     * @param Hasher $hasher
     * @param Dispatcher $dispatcher
     * @param string $model
     */
    public function __construct(
        Container $container,
        Hasher $hasher,
        Dispatcher $dispatcher,
        $model = '')
    {
        parent::__construct($hasher, $model);

        $this->container = $container;

        $this->dispatcher = $dispatcher;
    }

    /**
     * Returns the given instance or finds a user by their id
     *
     * @param $id
     * @return UserContract|null
     */
    public function retrieve($id)
    {
        if ($id instanceof UserContract)
        {
            return $id;
        }

        return $this->retrieveById($id);
    }

    public function register($attributes, $activate = false)
    {
        $this->fireEvent('SeptuRegisteringNewUser', compact('attributes'));

        $user = $this->create($attributes, $activate);

        $this->fireEvent('SeptuNewUserRegistered', compact('user'));

        return $user;
    }

    /**
     * Creates a new user
     *
     * @param $attributes
     * @param bool $activate
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($attributes, $activate = false)
    {
        $user = $this->createModel();

        $this->fireEvent('SeptuCreatingNewUser', $user);

        $this->ensureRequiredAttributesArePresent($attributes);

        $user->fill($attributes)->save();

        $this->fireEvent('SeptuFinishedCreatingUser', $user);

        $this->createActivation($user);

        if ( $activate === true)
        {
            $this->activate($user);
        }

        return $user;
    }

    /**
     * Updates an existing user
     *
     * @param UserContract|int $id
     * @param array $attributes
     * @param bool $activate
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, $attributes, $activate = false)
    {
        $user = $this->retrieve($id);

        $this->fireEvent('SeptuUpdatingUser', compact('user', 'attributes'));

        $this->ensureRequiredAttributesArePresent($attributes);

        $user->fill($attributes)->save();

        $this->fireEvent('SeptuFinishedUpdatingUser', compact('user', 'attributes'));

        return $user;
    }

    /**
     * Ensures that we have passed the required login and password fields.
     *
     * Any user that is created or updated must have at a minimum the login
     * column and password fields specified. If either is missing, and exception
     * will be thrown.
     *
     * An event is fired off here that will allow you to specify your own
     * set of required attributed if needed.
     *
     * Here is an example of using an event to add your own required fields.
     *
     * <code>
     * Event::listen('SeptuEnsuringRequiredFieldsArePresent', function($attributes)
     * {
     *     if (empty($attributes['gender']))
     *     {
     *         $exception = "You must specify your gender.";
     *         throw new InvalidArgumentException($exception);
     *     }
     * });
     * </code>
     *
     * @param $attributes
     * @throws InvalidArgumentException
     */
    protected function ensureRequiredAttributesArePresent($attributes)
    {
        $loginColumn = config('kyaris/septu::users.login_column', 'email');

        if (empty($attributes[$loginColumn]))
        {
            $exception = trans('kyaris/septu::users/message.error.no_login_column', ['column' => $loginColumn]);
            throw new InvalidArgumentException($exception);
        }

        if (empty($attributes['password']))
        {
            $exception = trans('kyaris/septu::users/message.error.no_password_column');
            throw new InvalidArgumentException($exception);
        }

        $this->fireEvent('SeptuEnsuringRequiredUserFieldsArePresent', compact('attributes'));
    }

    /**
     * Creates a new activation entry for the given user
     *
     * @param $id
     * @return mixed
     */
    public function createActivation($id)
    {
        $user = $this->retrieve($id);

        return $this->container['septu.activations']->create($id);
    }

    /**
     * {@inheritDoc}
     */
    public function activate($id)
    {
        $user = $this->retrieve($id);

        $activations = $this->container['septu.activations'];

        $this->dispatcher->fire('SeptuUserIsBeingActivated', $user);

        if ( ! $activation = $activations->completed($user))
        {
            if ( ! $activation = $activations->exists($user))
            {
                $activation = $this->createActivation($user);
            }

            $this->dispatcher->fire('SeptuUserHasBeenActivated', $user);

            $activation = $activations->complete($user, $activation->code);
        }

        return $activation;
    }

    /**
     * {@inheritDoc}
     */
    public function deactivate($id)
    {
        $user = $this->find($id);

        $this->dispatcher->fire('SeptuUserHasBeenDeactivated', $user);

        return $this->container['septu.activations']->remove($user);
    }
}