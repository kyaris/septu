<?php namespace Kyaris\Septu\Users\Mailers;

use Illuminate\Contracts\Auth\Authenticatable;
use Kyaris\Mailer\Mailer;

class UserMailer extends Mailer implements UserMailerInterface {

    /**
     * {@inheritDoc}
     */
    public function genericUserEmail(Authenticatable $user, $subject, $view, array $data = [])
    {
        $recipient = [
            'email' => $user->email,
            'name' => "{$user->first_name} {$user->last_name}"
        ];

        $this->sendEmail($subject, $recipient, $view, $data);
    }

    /**
     * {@inheritDoc}
     */
    public function adminApproval(Authenticatable $user, $subject, $view, array $data = [])
    {
        // Prepare the email recipient
        $recipient = [
            $this->config->get('mail.from.address'),
            $this->config->get('mail.from.name', null)
        ];

        // Prepare the email
        $this->sendEmail($subject, $recipient, $view, $data);
    }

    /**
     * Prepares the email by setting the necessary properties
     * and finally sends the email to the given recipient.
     *
     * @param  string  $subject
     * @param  array  $recipient
     * @param  string  $view
     * @param  array  $data
     * @return void
     */
    protected function sendEmail($subject, array $recipient, $view, array $data)
    {
        // Set the email subject
        $this->setSubject($subject);

        // Set the email recipient
        $this->addTo(
            array_get($recipient, 'email'),
            array_get($recipient, 'name')
        );

        // Set the email view
        $this->setView(
            "kyaris/septu::emails/{$view}",
            array_merge(compact('user'), $data)
        );

        // Send the email
        $this->send();
    }
}