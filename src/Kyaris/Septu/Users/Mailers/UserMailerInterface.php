<?php namespace Kyaris\Septu\Users\Mailers;

use Illuminate\Contracts\Auth\Authenticatable;

interface UserMailerInterface {

    /**
     * Send a generic email to the user.
     *
     * @param  Authenticatable $user
     * @param  string  $subject
     * @param  string  $view
     * @param  array  $data
     * @return void
     */
    public function genericUserEmail(Authenticatable $user, $subject, $view, array $data = []);

    /**
     * Send the application administrator an email for user account activation.
     *
     * @param  Authenticatable  $user
     * @param  string  $subject
     * @param  string  $view
     * @param  array  $data
     * @return void
     */
    public function adminApproval(Authenticatable $user, $subject, $view, array $data = []);

}