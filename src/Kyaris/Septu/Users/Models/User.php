<?php namespace Kyaris\Septu\Users\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Kyaris\Septu\Acl\Permissions\Contracts\PermissibleInterface;
use Kyaris\Septu\Acl\Permissions\Permissions;
use Kyaris\Septu\Roles\Traits\RoleableTrait;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, PermissibleInterface {

    use Authenticatable, CanResetPassword, RoleableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * @{inheritDoc}
     */
    protected $guarded = ['id', 'created_at', 'updated_at', 'remember_token'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Array containing dynamic non-column related accessors
     *
     * @var array
     */
    protected $appends = ['role_permissions'];

    /**
     * Set `password` mutator.
     *
     * @param  string   $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * Get mutator for the "permissions" attribute.
     *
     * @param  mixed  $permissions
     * @return array
     */
    public function getPermissionsAttribute($permissions)
    {
        return $permissions ? json_decode($permissions, true) : [];
    }

    /**
     * Set mutator for the "permissions" attribute.
     *
     * @param  mixed  $permissions
     * @return void
     */
    public function setPermissionsAttribute(array $permissions)
    {
        $this->attributes['permissions'] = $permissions ? json_encode($permissions) : '';
    }

    /**
     * Creates our permissions instance
     *
     * @return mixed
     */
    public function createPermissions()
    {
        return new Permissions($this->permissions, $this->role_permissions);
    }

    /**
     * Gathers our permissions granted by our roles.
     *
     * @return mixed
     */
    public function getRolePermissionsAttribute()
    {
        $rolePermissions = $this->roles->map(function($role)
        {
            return $role->permissions;
        });

        return $rolePermissions->toArray();
    }

    /**
     * Dynamically pass missing methods to the user.
     *
     * @param  string $method
     * @param  array $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        $methods = [ 'hasAccess', 'hasAnyAccess', 'getPermissions' ];

        if ( in_array($method, $methods) )
        {
            $permissions = $this->createPermissions();

            return call_user_func_array([ $permissions, $method ], $parameters);
        }

        return parent::__call($method, $parameters);
    }

}