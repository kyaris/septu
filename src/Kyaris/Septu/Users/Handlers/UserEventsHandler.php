<?php namespace Kyaris\Septu\Users\Handlers;

use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Events\Dispatcher;
use Kyaris\Support\Traits\Eventable;

class UserEventsHandler {

    use Eventable;

    /**
     * Illuminate Application Container
     *
     * @var Container
     */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function subscribe(Dispatcher $dispatcher)
    {
        $dispatcher->listen('SeptuRegisteringNewUser', __CLASS__ . '@registeringNewUserAccount');
        $dispatcher->listen('SeptuNewUserRegistered', __CLASS__ . '@newUserAccountRegistered');
    }

    /**
     * Events fired before a new user is registered
     *
     * @param array $attributes
     */
    public function registeringNewUserAccount(array $attributes)
    {

    }

    /**
     * Events fired when a new user has been registered
     *
     * @param Authenticatable $user
     */
    public function newUserAccountRegistered(Authenticatable $user)
    {
        $container = $this->container;

        $subject = trans('kyaris/septu::email.subject.welcome', [
            'siteName' => $container['config']->get('site.title')
        ]);

        $activationProvider = $container['septu.activations'];

        switch (config('kyaris/septu::activations.type'))
        {
            case 'email':

                $activation = $activationProvider->create($user);

                $activationLink = url(config('kyaris/septu::activations.uri'), [$user->id, $activation->code]);

                $this->getMailer()->genericUserEmail($user, $subject, 'user_welcome_inactive', compact('user', 'activationLink'));

                break;

            case 'admin':

                $activation = $activationProvider->create($user);

                $activationLink = url(config('kyaris/septu::activations.uri'), [$user->id, $activation->code]);

                $this->getMailer()->genericUserEmail($user, $subject, 'user_welcome_admin_required');
                $this->getMailer()->adminApproval($user, $subject, 'admin_activate', compact('user', 'activationLink'));

                break;

            default:

                $this->getMailer()->genericUserEmail($user, $subject, 'user_welcome');

                break;
        }
    }

    /**
     * Returns the mailer instance.
     *
     * @return \Kyaris\Septu\Users\Mailers\UserMailerInterface
     */
    protected function getMailer()
    {
        return $this->container['septu.users.mailer'];
    }
}