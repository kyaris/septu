<?php namespace Kyaris\Septu;

use Illuminate\Support\ServiceProvider;
use InvalidArgumentException;
use Kyaris\Septu\Activations\Repositories\EloquentActivationRepository;
use Kyaris\Septu\Barriers\ActivationBarrier;
use Kyaris\Septu\Resources\ResourceBag;
use Kyaris\Septu\Resources\Resources;
use Kyaris\Septu\Roles\Repositories\EloquentRoleRepository;
use Kyaris\Septu\Users\Handlers\UserEventsHandler;
use Kyaris\Septu\Users\Mailers\UserMailer;
use Kyaris\Septu\Users\Repositories\EloquentUserRepository;

class SeptuServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Route middleware keys.
	 *
	 * @var array
	 */
	protected $middleware = [
		'septu.permissions' => 'Kyaris\Septu\Acl\Middleware\PermissionsMiddleware',
	];

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerBindings();
		$this->registerUsers();
		$this->registerRoles();
		$this->registerActivations();
		$this->registerBarriers();
		$this->registerUserMailer();
		$this->registerUserEventsHandler();
		$this->registerResources();
		$this->registerSeptu();
		$this->registerMiddleware();
		$this->registerRoutes();
	}

	/**
	 * Boots our services
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('kyaris/septu', 'kyaris/septu');

		$this->extendAuthProvider();

		$this->app['events']->subscribe('septu.users.events');
	}

	/**
	 * Registers our user provider
	 */
	public function registerUsers()
	{
		$this->app['septu.users'] = $this->app->share(function($app)
		{
			$model = $app['config']['kyaris/septu::users.model'];

			return new EloquentUserRepository($app, $app['hash'], $app['events'], $model);
		});
	}

	/**
	 * Registers our roles repository with the container
	 */
	protected function registerRoles()
	{
		$this->app['septu.roles'] = $this->app->share(function($app)
		{
			$model = $app['config']['kyaris/septu::roles.model'];

			return new EloquentRoleRepository($app['events'], $model);
		});
	}

	/**
	 * Registers our activation repository
	 */
	protected function registerActivations()
	{
		$this->app['septu.activations'] = $this->app->share(function($app) {

			$model = $app['config']['kyaris/septu::activations.model'];
			$expires = $app['config']['kyaris/septu::activations.expires'];

			return new EloquentActivationRepository($model, $expires);
		});
	}

	/**
	 * Registers our user mailer with the container
	 */
	protected function registerUserMailer()
	{
		$this->app['septu.users.mailer'] = $this->app->share(function($app) {

			return new UserMailer($app['mailer'], $app['config']);
		});
	}

	/**
	 * Registers our user events handler with the container
	 */
	protected function registerUserEventsHandler()
	{
		$this->app['septu.users.events'] = $this->app->share(function($app)
		{
			return new UserEventsHandler($app);
		});
	}

	/**
	 * Registers the activation checkpoint.
	 *
	 * @return void
	 */
	protected function registerActivationBarrier()
	{
		if  ( ! isset($this->app['septu.activations']))
		{
			$this->registerActivations();
		}

		$this->app['septu.barriers.activation'] = $this->app->share(function($app)
		{
			return new ActivationBarrier($app['septu.activations']);
		});
	}

	/**
	 * Registers our packaged barriers.
	 *
	 * @return void
	 * @throws InvalidArgumentException
	 */
	protected function registerBarriers()
	{
		$this->registerActivationBarrier();

		$this->app['septu.barriers'] = $this->app->share(/**
		 * @param $app
		 * @return array
         */
		function($app)
		{
			$activeBarriers = $app['config']['kyaris/septu::barriers'];

			$barriers = [];

			foreach ($activeBarriers as $barrier)
			{
				if ( ! $app->offsetExists("septu.barriers.{$barrier}"))
				{
					throw new InvalidArgumentException("Invalid barrier [$barrier] given.");
				}

				$barriers[$barrier] = $app["septu.barriers.{$barrier}"];
			}

			return $barriers;
		});
	}

	/**
	 * Extends Illuminates Auth to include Septu as a Driver
	 */
	public function extendAuthProvider()
	{
		$this->app['auth']->extend('septu', function($app)
		{
			return $app['septu'];
		});
	}

	/**
	 * Registers our routes file
	 */
	protected function registerRoutes()
	{
		// $this->app['files']->requireOnce(__DIR__ . '/../../routes.php');
	}

	/**
	 * Registers our resource bag
	 */
	protected function registerResources()
	{
		$this->app['septu.resources'] = $this->app->share(function($app)
		{
			$bag = new ResourceBag;

			return new Resources($bag);
		});
	}

	/**
	 * Registers septu with the container
	 */
	protected function registerSeptu()
	{
		$this->app['septu'] = $this->app->share(function($app)
		{
			$septu = new Septu(
				$app['septu.users'],
				$app['septu.roles'],
				$app['septu.activations'],
				$app['session.store'],
				$app['request']
			);

			$septu->setDispatcher($app['events']);

			if (isset($app['septu.barriers']))
			{
				foreach ($app['septu.barriers'] as $key => $checkpoint)
				{
					$septu->enableBarrier($key, $checkpoint);
				}
			}

			return $septu;
		});
	}

	/**
	 * Binds our interfaces in the IoC
	 */
	protected function registerBindings()
	{
		$app = $this->app;

		$app->bindIf(
			'Kyaris\Septu\Users\Repositories\UserRepositoryInterface',
			'Kyaris\Septu\Users\Repositories\EloquentUserRepository'
		);

		$app->bindIf(
			'Kyaris\Septu\Roles\Repositories\RoleRepositoryInterface',
			'Kyaris\Septu\Roles\Repositories\EloquentRoleRepository'
		);

		$app->bindIf(
			'Kyaris\Septu\Activations\Repositories\ActivationRepositoryInterface',
			'Kyaris\Septu\Activations\Repositories\EloquentActivationRepository'
		);
	}

	protected function registerMiddleware()
	{
		$router = $this->app['router'];

		foreach ($this->middleware as $key => $class)
		{
			$router->middleware($key, $class);
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [
			'septu.users',
			'septu.roles',
			'septu'
		];
	}

}
